#include "helper.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


/* 81 = 8.1%, 128 = 12.8% and so on. The 27th frequency is the space. Source is Wikipedia */
int english_letter_frequencies [ 27 ] = 
{
	81, 15, 28, 43, 128, 23, 20, 61, 71,  
	2,   1, 40, 24,  69, 76, 20,  1, 61, 
	64, 91, 28, 10,  24,  1, 20,  1, 130
};


typedef struct node_t node_t;
struct node_t
{
	char character;
	int  frequency;
	node_t *left;
	node_t *right;
};


typedef struct min_heap_t
{
	size_t size;
	size_t capacity;
	node_t **node_array;
} min_heap_t;



node_t *new_node ( char character, size_t frequency )
{
	node_t *node = ( node_t* ) malloc ( sizeof ( node_t ) );
	assert ( node );
	node->left      = node->right = 0;
	node->character = character;
	node->frequency = frequency;
	return node;
}


min_heap_t *create_mean_heap ( size_t capacity )
{
	min_heap_t *mh = ( min_heap_t* ) malloc ( sizeof ( min_heap_t ) );
	assert ( mh );
	
	mh->size     = 0;
	mh->capacity = capacity;
	mh->array = ( min_heap_t** ) malloc ( mh->capacity * sizeof ( min_heap_t ) );
	assert ( mh->array );
	return mh;
}


void swap_node ( node_t **a, node_t **b )
{
	node_t *t = *b;
	*b = *a;
	*a =  t;
	return;
}

bool mean_heap_check_size_one ( min_heap_t* mh )
{
	return ( mh->size == 1 );
}


void heapify ( min_heap_t *mh, int index )
{
	
}


i32 main ( i32 argc, i8** argv )
{
	
	
	
	return 0;
}

